#!/usr/bin/env python3

import datetime
import pytz

class Wallet:
    """Creating a crypto currency wallet class"""

    def __init__(self, address, balance):
        self.address = address
        self.balance = balance
        print("New wallet address: " + self.address)
        print("Opening Balance, current balance:")

    def buy(self, ammount):
        if ammount > 0:
            self.balance += ammount
            self.show_balance()


    def send(self, ammount):
        if 0 < ammount <= self.balance:
            self.balance -= ammount
        else:
            print("Amount must be greater than zero and not more than your current balance")
        self.show_balance()

    def show_balance(self):
        print("{}".format(self.balance))



if __name__ == '__main__':
    sc3498a459106a5bed79bb = Wallet("sc3498a459106a5bed79bb", 0.000000)
    sc3498a459106a5bed79bb.show_balance()


    sc3498a459106a5bed79bb.buy(2.530000)
    #sc3498a459106a5bed79bb.show_balance
    sc3498a459106a5bed79bb.send(1.300000)
    #sc3498a459106a5bed79bb.show_balance





